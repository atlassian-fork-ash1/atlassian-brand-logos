var less = require('gulp-less');
var gulp = require('gulp');

var lessOptions = {
    modifyVars: {
        'imagepath': '..'
    }
};

function buildLess() {
    return gulp.src('src/**/*.less')
        .pipe(less(lessOptions))
        .pipe(gulp.dest('./dist'));
}

gulp.task('less', buildLess);
